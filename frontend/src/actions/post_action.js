import { GET_ALL_POST_SUCCESS, GET_POST_BY_ID } from "../constant";
import authAxios from "../helper/authAxios";

export const get_single_post = (post_id) => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/posts/${post_id}`);
    if (data.status == "success" && data.data.length > 0)
      dispatch({ type: GET_POST_BY_ID, data: data.data[0] });
  } catch (err) {
    console.log(err);
  }
};

export const get_posts_for_teacher = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/posts/teacher`);
    if (data.status == "success")
      dispatch({ type: GET_ALL_POST_SUCCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};
export const get_recent_and_famous_posts = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/posts/recent`);
    if (data.status == "success")
      dispatch({ type: GET_ALL_POST_SUCCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};
export const get_posts_by_search_tage =
  ({ search = "", tags = "" }) =>
  async (dispatch) => {
    try {
      const { data } = await authAxios().get(
        `/posts/search?search=${search}&tags=${tags}&teacher=true`
      );

      if (data.status == "success")
        dispatch({ type: GET_ALL_POST_SUCCESS, data: data.data });
    } catch (err) {
      console.log(err);
    }
  };
export const add_post = (post, navigate) => async (dispatch) => {
  try {
    const { data } = await authAxios().post(`/posts`, post);

    if (data.status == "success") navigate("/posts");
  } catch (err) {
    console.log(err);
  }
};

export const get_post_for_update = (post_id) => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/posts/update/${post_id}`);
    if (data.status == "success")
      dispatch({ type: GET_POST_BY_ID, data: data.data });
  } catch (err) {
    console.log(err);
  }
};
export const update_post = (post, navigate) => async (dispatch) => {
  try {
    const { data } = await authAxios().put(`/posts`, post);
    console.log(data);
    if (data.status == "success") navigate("/posts");
  } catch (err) {
    console.log(err);
  }
};
