import authAxios from "../helper/authAxios";

export const send_email = async (obj) => {
  try {
    const { data } = await authAxios().post(`/send_email/request`, obj);
    console.log(data);
    return data;
  } catch (err) {
    console.log(err);
  }
};
