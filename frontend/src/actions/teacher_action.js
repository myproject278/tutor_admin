import {
  ADD_REVIEW_SUCCESS,
  GET_REVIEW_SUCCESS,
  GET_TEACHERS_LIST,
  GET_TEACHER_BY_ID,
} from "../constant";
import authAxios from "../helper/authAxios";

export const get_teacher_list = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/teacher`);
    console.log(data);
    if (data.status == "success")
      dispatch({ type: GET_TEACHERS_LIST, data: data.data });
  } catch (err) {
    console.log(err);
  }
};

export const get_teacher_by_id = (teacher_id) => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/teacher/${teacher_id}`);
    console.log(data);
    if (data.status == "success")
      dispatch({ type: GET_TEACHER_BY_ID, data: data.data });
  } catch (err) {
    console.log(err);
  }
};

export const get_teacher_for_request_form =
  (teacher_id) => async (dispatch) => {
    try {
      const { data } = await authAxios().get(
        `/teacher/request_form/${teacher_id}`
      );
      if (data.status == "success")
        dispatch({ type: GET_TEACHER_BY_ID, data: data.data });
    } catch (err) {
      console.log(err);
    }
  };



export const get_top_teacher = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/teacher/top3`);
    if (data.status == "success")
      dispatch({ type: GET_TEACHERS_LIST, data: data.data });
  } catch (err) {
    console.log(err);
  }
};
