import { GET_ADDRESS_FOR_USER_SUCCESS } from "../constant";
import authAxios from "../helper/authAxios";

export const get_address_by_user_id = (user_id) => async dispatch => {
    try {
       
        const {data}= await authAxios().get(`/address/user/${user_id}`)
        if(data.status=='success')
        dispatch({type:GET_ADDRESS_FOR_USER_SUCCESS , data : data.data})
       }  catch(err){
        console.log(err)
       }
}