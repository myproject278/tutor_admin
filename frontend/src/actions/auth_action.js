import axios from "axios";
import {
  AUTH_LOADING,
  AUTH_LOADING_COMPLETE,
  GET_ALL_TEACHER_DETAILS,
  LOGOUT,
  SIGN_IN_UP,
  SIGN_UP_IN_FAILURE,
} from "../constant";
import authAxios from "../helper/authAxios";

import { BASE_URL } from "../constant";
export const signUp = (formData) => async (dispatch) => {
  try {
    console.log(formData);
    const { data } = await axios.post(`${BASE_URL}/teacher/signup`, formData);
    saveData(data, dispatch);
  } catch (err) {
    console.log(err);
  }
};

export const signIn = (user_data) => async (dispatch) => {
  try {
    const { data } = await axios.post(`${BASE_URL}/teacher/signin`, user_data);
    saveData(data, dispatch);
  } catch (err) {
    dispatch({
      type: SIGN_IN_UP,
      payload: {
        error: err,
      },
    });
  }
};
const saveData = (data, dispatch) => {
  if (data.status == "success") {
    const user = data.data;

    localStorage.setItem("teacher", JSON.stringify(user));
    localStorage.setItem("teacher_token", data.token);
    console.log(data);
    dispatch({
      type: SIGN_IN_UP,
      payload: {
        user,
        token: data.token,
      },
    });
  } else {
    console.log(data);
    dispatch({ type: SIGN_UP_IN_FAILURE, error: data.error });
  }
};

export const isUserLoggedIn = () => (dispatch) => {
  const token = localStorage.getItem("teacher_token");
  if (token) {
    const user = JSON.parse(localStorage.getItem("teacher"));
    dispatch({
      type: SIGN_IN_UP,
      payload: {
        user,
        token,
      },
    });
  }
  dispatch({ type: AUTH_LOADING_COMPLETE });
};

export const get_all_teacher_info = (teacher_id) => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`${BASE_URL}/teacher/${teacher_id}`);
    dispatch({ type: GET_ALL_TEACHER_DETAILS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};

export const logoutUser = () => async (dispatch) => {
  console.log("logout");
  localStorage.clear();
  dispatch({ type: LOGOUT });
};

export const update_teacher = (obj) => async (dispatch) => {
  try {
    console.log(obj);
    const { data } = await authAxios().put(`${BASE_URL}/teacher`, obj);
    console.log(data);
    if (data.status == "success")
      dispatch({ type: GET_ALL_TEACHER_DETAILS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};