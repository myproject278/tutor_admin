import { FETCH_SUBJECTS, FETCH_SUBJECTS_FOR_DROPDOWN } from "../constant";
import authAxios from "../helper/authAxios";

export const getSubjectsForDropdown = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/subject/dropdown`);
    if (data.status == "success")
      dispatch({ type: FETCH_SUBJECTS_FOR_DROPDOWN, data: data.data });
  } catch (err) {
    console.log(err);
  }
};
export const getTopSubjects = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/subject/top4`);
    if (data.status == "success")
      dispatch({ type: FETCH_SUBJECTS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};
