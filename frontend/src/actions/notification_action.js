import {
  GET_NOTIFICATION_SUCESS,
  REDUCE_UNREAD_COUNT,
  SET_UNREAD_NOTIFICATION_COUNT,
} from "../constant";
import authAxios from "../helper/authAxios";

export const get_notifications = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/notifications/0`);
    console.log(data);
    if (data.status == "success")
      dispatch({ type: GET_NOTIFICATION_SUCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};

export const handle_notification_read = (obj) => async (dispatch) => {
  try {
    const { data } = await authAxios().put(`/notifications`, obj);
    if (data.status == "success") dispatch({ type: REDUCE_UNREAD_COUNT });
  } catch (err) {
    console.log(err);
  }
};

export const get_unread_notification_count = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/notifications/count/0`);

    if (data.status == "success")
      dispatch({ type: SET_UNREAD_NOTIFICATION_COUNT, count: data.data.count });
  } catch (err) {
    console.log(err);
  }
};
