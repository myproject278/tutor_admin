import { ADD_TRANSACTION_FROM_USER,GET_EXPENDITURE_DATE_SUCCESS,GET_OWNER_SUGGESTION,GET_TOTAL_PAID_AMT,GET_TOTAL_TRANSACTIONS_COUNT,GET_TRANSACTIONS_FOR_USER, GET_USER_OWNER_DUE } from "../constant";
import authAxios from "../helper/authAxios";
export const add_transaction = async (transaction,navigate) =>{
    try{
        const {data}= await authAxios().post(`/transaction`,transaction)
        console.log(data)
        if(data.status=='success')
          navigate('/transactions')
       
       }  catch(err){
        console.log(err)
       }
}

export const get_transaction_for_user =  (page,rowsPerPage) => async dispatch =>{
  try{
      
      const {data}= await authAxios().get(`/transaction/user/${page}/${rowsPerPage}`)
      
      if(data.status=='success')
      dispatch({type:GET_TRANSACTIONS_FOR_USER , data : data.data})
     
     }  catch(err){
      console.log(err)
     }
}

export const get_transaction_for_user_owner =  (owner_id,page,rowsPerPage) => async dispatch =>{
    try{
        
        const {data}= await authAxios().get(`/transaction/user/owner/${owner_id}/${page}/${rowsPerPage}`)
        if(data.status=='success')
        dispatch({type:GET_TRANSACTIONS_FOR_USER , data : data.data})
       
       }  catch(err){
        console.log(err)
       }
  }


  export const get_owener_suggetions = ()=> async dispatch=>{
    try{
        
        const {data}= await authAxios().get('/user/owner')
        dispatch({type:GET_OWNER_SUGGESTION , data : data.data})
       }  catch(err){
        console.log(err)
       }
  }
  

export const get_user_owner_due =  (page,rowsPerPage) => async dispatch =>{
    try{
        
        const {data}= await authAxios().get(`/transaction/user/due/${page}/${rowsPerPage}`)
        
        if(data.status=='success')
        dispatch({type:GET_USER_OWNER_DUE , data : data.data})
       
       }  catch(err){
        console.log(err)
       }
  }
  export const get_count_of_user_owner_due_table = ()=> async dispatch =>{
    try{
        
      const {data}= await authAxios().get(`/transaction/user/due_count`)
      console.log('data')
      console.log(data)
      if(data.status=='success')
       dispatch({type:GET_TOTAL_TRANSACTIONS_COUNT, data:data.data})
      else 
      console.log(data)
     }  catch(err){
      console.log(err)
     }
  }

  export const get_transactions_on_date =  ({start_date,end_date}) => async dispatch =>{
    try{
        if(start_date && end_date){
        const {data}= await authAxios().get(`/transaction/user/date/${start_date}/${end_date}`)
            if(data.status=='success')
             dispatch({type:GET_EXPENDITURE_DATE_SUCCESS,data:{expenditure:data.data[0].sum_expenditure,paid:data.data[0].sum_paid}})
            else
             console.log(data)
          
        }
       
       }  catch(err){
        console.log(err)
       }
  }

  export const get_count_for_user = ()=>async dispatch=>{
    try{
        
      const {data}= await authAxios().get(`/transaction/user/count`)
      if(data.status=='success')
       dispatch({type:GET_TOTAL_TRANSACTIONS_COUNT, data:data.data})
      else 
      console.log(data)
     }  catch(err){
      console.log(err)
     }
  }

  export const get_count_for_user_owner = (id )=>async dispatch=>{
    try{
      const {data}= await authAxios().get(`/transaction/user/count/${id}`)
      if(data.status=='success')
      dispatch({type:GET_TOTAL_TRANSACTIONS_COUNT, data:data.data})
     else 
     console.log(data)
     }  catch(err){
      console.log(err)
     }
  }


  export const get_total_paid_amount= ()=>async dispatch =>{
    try{
      const {data}= await authAxios().get(`/transaction/user/total_paid`)
      console.log('ada')
      console.log(data)
      if(data.status=='success')
      dispatch({type:GET_TOTAL_PAID_AMT, data:data.data})
     else 
     console.log(data)
     }  catch(err){
      console.log(err)
     }
  }