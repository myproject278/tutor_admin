import {
  GET_SUBJECTS_FOR_TEACHER,
  GET_SUBJECT_DETAILS_FOR_TEACHER_UPDATE,
  GET_SUBJECT_TEACHER_BY_ID,
  GET_TEACHERS_BY_SUBJECT,
} from "../constant";
import authAxios from "../helper/authAxios";
import store from "../store/store";
export const get_teachers_by_subject =
  (
    subject_id,
    sorting_column = "id",
    page_id,
    daysAvailability,
    expertiseLevel,
    searchText
  ) =>
  async (dispatch) => {
    try {
      const { data } = await authAxios().get(
        `/subject/teacher/${subject_id}/${sorting_column}/${page_id}?days_of_availability=${daysAvailability}&expertise_level=${expertiseLevel}&teacher_name=${searchText}`
      );
      if (data.status == "success")
        dispatch({ type: GET_TEACHERS_BY_SUBJECT, data: data.data });
    } catch (err) {
      console.log(err);
    }
  };

export const get_subject_teacher_details_by_id =
  ({ subject_id, teacher_id }) =>
  async (dispatch) => {
    try {
      const { data } = await authAxios().get(
        `/subject/teacher/${subject_id}/${teacher_id}`
      );
      console.log(data);
      if (data.status == "success")
        dispatch({ type: GET_SUBJECT_TEACHER_BY_ID, data: data.data });
    } catch (err) {
      console.log(err);
    }
  };

export const get_subjects_for_teacher = (teacher_id) => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/subject/teacher/subject_list`);
    const expertiese_level_arr = ["Basic", "Intermediate", "Expert"];
    if (data.status == "success")
      dispatch({
        type: GET_SUBJECTS_FOR_TEACHER,
        data: data.data.map((st) => ({
          ...st,
          level_of_knowledge: expertiese_level_arr[st.level_of_knowledge - 1],
        })),
      });
  } catch (err) {
    console.log(err);
  }
};

export const add_subjects_for_teacher =
  (subject, navigate) => async (dispatch) => {
    try {
      const { data } = await authAxios().post(`/subject/teacher`, subject);
      if (data.status == "success") navigate("/subjects");
    } catch (err) {
      console.log(err);
    }
  };

export const get_subject_info_for_update = (subject_id) => async (dispatch) => {
  try {
    console.log("in asdifas dfa");
    const { data } = await authAxios().get(
      `/subject/teacher/update/${subject_id}`
    );
    console.log(data);
    if (data.status == "success")
      dispatch({
        type: GET_SUBJECT_DETAILS_FOR_TEACHER_UPDATE,
        data: data.data,
      });
  } catch (err) {
    console.log(err);
  }
};

export const update_subjects_for_teacher =
  (subject, navigate) => async (dispatch) => {
    try {
      const { data } = await authAxios().put(`/subject/teacher`, subject);
      if (data.status == "success") navigate("/subjects");
    } catch (err) {
      console.log(err);
    }
  };
