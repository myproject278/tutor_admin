import { GET_COMMNETS_SUCESS } from "../constant";
import authAxios from "../helper/authAxios";

export const add_comment = (comments) => async (dispatch) => {
  try {
    const { data } = await authAxios().post(`/comments`, comments);
  } catch (err) {
    console.log(err);
  }
};
export const get_comments_by_post = (post_id) => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/comments/${post_id}`);
    if (data.status == "success")
      dispatch({ type: GET_COMMNETS_SUCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};
