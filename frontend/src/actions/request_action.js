import {
  GET_REQUEST_BY_ID_SUCESS,
  GET_REQUEST_BY_USER_ID,
  GET_REQUEST_FOR_CALENDER,
} from "../constant";
import authAxios from "../helper/authAxios";
export const add_request = async (request, navigate) => {
  try {
    console.log(request);
    const { data } = await authAxios().post(`/request`, request);
    if (data.status == "success") navigate("/requests");
  } catch (err) {
    console.log(err);
  }
};

export const get_request_by_teacher_id =
  (searchQuery, checkedStatus) => async (dispatch) => {
    try {
      const { data } = await authAxios().get(`/request/teacher`);
      if (data.status == "success") {
        const statusOptions = [
          "Requested",
          "Approved",
          "Payment",
          "Completed",
          "Closed",
          "Canceled",
        ];
        dispatch({
          type: GET_REQUEST_BY_USER_ID,
          data: data.data.map((r) => ({
            ...r,
            status: statusOptions[r.status],
          })),
        });
      }
    } catch (err) {
      console.log(err);
    }
  };

export const get_request_by_id = (id) => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/request/student/${id}`);
    console.log(data);
    if ((data.status = "success"))
      dispatch({ type: GET_REQUEST_BY_ID_SUCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};

export const hanndle_approve_slot = async (obj, request_id) => {
  try {
    const { data } = await authAxios().put(`/request/${request_id}`, obj);
    return data;
  } catch (err) {
    console.log(err);
  }
};

export const get_request_for_teacher_calender = (id) => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/request/teacher/calender`);
    if (data.status == "success") {
      dispatch({
        type: GET_REQUEST_FOR_CALENDER,
        data: data.data,
      });
    }
  } catch (err) {
    console.log(err);
  }
};
