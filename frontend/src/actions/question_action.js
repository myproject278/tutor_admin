import {
  GET_ALL_POST_SUCCESS,
  GET_ANSWER_FOR_QUESTION,
  GET_POST_BY_ID,
  GET_QUESTIONS,
} from "../constant";
import authAxios from "../helper/authAxios";
import store from "../store/store";

export const add_question = (question) => async (dispatch) => {
  try {
    const user = store.getState().authReducer.user;
    console.log(user);
    const { data } = await authAxios().post(`/qa/question`, {
      type: "student",
      name: user.name,
      image_url: user.image_url,
      qa_text: question,
    });

    console.log(data);
  } catch (err) {
    console.log(err);
  }
};

export const add_answer_for_question =
  (question_id, answer) => async (dispatch) => {
    try {
      const user = store.getState().authReducer.user;
      console.log(user);
      const { data } = await authAxios().post(`/qa/answer`, {
        type: "student",
        name: user.name,
        image_url: user.image_url,
        qa_text: answer,
        id: question_id,
      });

      console.log(data);
    } catch (err) {
      console.log(err);
    }
  };

export const get_questions = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/qa/questions`);
    if (data.status == "success")
      dispatch({ type: GET_QUESTIONS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};

export const get_answers_by_questions = (question_id) => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/qa/answer/${question_id}`);
    console.log(data);
    if (data.status == "success")
      dispatch({ type: GET_ANSWER_FOR_QUESTION, data: data.data });
  } catch (err) {
    console.log(err);
  }
};
