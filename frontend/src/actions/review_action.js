import { ADD_REVIEW_SUCCESS, GET_REVIEW_SUCCESS } from "../constant"
import authAxios from "../helper/authAxios"


export const add_review = (review,setOpen) => async dispatch => {
    try{
        const {data}= await authAxios().post(`/review`,review)
        console.log(data)
        if (data.status == 'success') {
            console.log('in success')
            setOpen(false)
            // dispatch(get_single_review(review.get('product_id')))
        }
       
       }  catch(err){
        console.log(err)
       }
}
export const get_single_review = (product_id) => async dispatch => {
    try{
        
        const { data } = await authAxios().get(`/review/single/${product_id}`)
      

        if (data.status == 'success' && data.data.length>0)
            dispatch({type:ADD_REVIEW_SUCCESS,data:data.data[0]})
       
       }  catch(err){
        console.log(err)
       }
}

export const get_review_for_teacher = ({ teacher_id, subject_id }) => async dispatch=>{
    try{
        console.log(teacher_id)
        console.log(subject_id)
        const { data } = await authAxios().get(`/review/${subject_id}/${teacher_id}`)
       console.log('data')
        console.log(data)
        if (data.status == 'success')
            dispatch({type:GET_REVIEW_SUCCESS,data:data.data})
       
       }  catch(err){
        console.log(err)
       }
}