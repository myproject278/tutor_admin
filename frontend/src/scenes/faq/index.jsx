import React, { useState } from "react";
import { Box, useTheme } from "@mui/material";
import Header from "../../components/Header";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { tokens } from "../../theme";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import {
  get_questions,
  add_answer_for_question,
  get_answers_by_questions,
} from "../../actions/question_action";
import { Avatar, TextField, Button } from "@material-ui/core";
import { get_image_url } from "../../helper/util";
import { formatDate } from "../../utils/functions";
const FAQ = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const dispatch = useDispatch();
  const [answer, setAnswer] = useState({});

  const { questions, answers } = useSelector((state) => state.questionReducer);
  const [expandedPanel, setExpandedPanel] = useState(null); // State to track expanded panel

  useEffect(() => {
    dispatch(get_questions());
  }, []);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpandedPanel(isExpanded ? panel : null);
    if (isExpanded) {
      dispatch(get_answers_by_questions(panel));
    }
  };
  const add_answer = (e, id) => {
    setAnswer({ ...answer, [id]: e?.target?.value });
  };
  const submit_answer = (question_id) => {
    dispatch(add_answer_for_question(question_id, answer[question_id]));
    setAnswer({ ...answer, [question_id]: "" });
  };
  return (
    <Box m="20px">
      <Header title="FAQ" subtitle="Asked Questions Page" />

      {questions.map(
        (
          { date, id, image_url, name, parent_id, qa_text, type, user_id },
          index
        ) => (
          <Accordion
            key={index}
            expanded={expandedPanel === id}
            onChange={handleChange(id)}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              style={{ width: "100%" }}
            >
              <div style={{ width: "100%", margin: "0 20px" }}>
                <div style={{ display: "flex" }}>
                  <Typography style={{ marginRight: "20px" }}>
                    {name} &nbsp;-
                  </Typography>
                  <Typography color={colors.greenAccent[500]} variant="h5">
                    {qa_text}
                  </Typography>
                </div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "flex-end",
                    width: "100%",
                  }}
                >
                  <Typography>{formatDate(date)}</Typography>
                </div>
              </div>
            </AccordionSummary>
            <AccordionDetails>
              <div>
                {answers.length > 0 && (
                  <>
                    <div
                      className="text"
                      style={{
                        maxHeight: "60vh",
                        overflow: "hidden", // Hide scroll bar initially
                        overflowY: answers.length > 0 ? "scroll" : "hidden",
                      }}
                    >
                      <div>
                        {answers.map((an) => (
                          <>
                            <div style={{ padding: "10px 24px" }}>
                              <Typography
                                style={{
                                  fontSize: "18px",
                                  padding: "7px",
                                }}
                              >
                                {an.qa_text}
                              </Typography>
                              <div
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                  justifyContent: "space-between",
                                }}
                              >
                                <div className="flex">
                                  <Typography style={{ color: "#7d7d7d" }}>
                                    answered :&nbsp;
                                    {formatDate(an.date)}{" "}
                                  </Typography>
                                </div>
                                <div
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                  }}
                                >
                                  <Avatar src={get_image_url(an.image_url)} />
                                  <Typography style={{ margin: "0 10px" }}>
                                    {an.name}
                                  </Typography>
                                </div>
                              </div>
                              <hr style={{ color: "#7d7d7d" }} />
                            </div>
                          </>
                        ))}
                      </div>
                    </div>
                  </>
                )}
                <div
                  style={{
                    display: "grid",
                    gridTemplateColumns: "80% 20%",
                    gap: "10px",
                    padding: "22px 30px",
                  }}
                >
                  <TextField
                    placeholder="enter answer...."
                    value={answer[id]}
                    onChange={(e) => add_answer(e, id)}
                  />
                  <Button
                    style={{
                      height: "30px",
                      margin: "0 58px",
                      width: "92px",
                      color: "#7d7d7d",
                    }}
                    variant="outlined"
                    onClick={() => submit_answer(id)}
                  >
                    Submit
                  </Button>
                </div>
              </div>
            </AccordionDetails>
          </Accordion>
        )
      )}
    </Box>
  );
};

export default FAQ;
