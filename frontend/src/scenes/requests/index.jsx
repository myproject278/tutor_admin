import { Box } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import { mockDataContacts } from "../../data/mockData";
import Header from "../../components/Header";
import { useTheme } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { get_request_by_teacher_id } from "../../actions/request_action";
import { useNavigate } from "react-router-dom";
import { format } from "date-fns";

const Reqeusts = () => {
  const requests = useSelector((state) => state.requestReducer.requests);
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(get_request_by_teacher_id());
  }, []);
  console.log(requests);
  const columns = [
    { field: "id", headerName: "ID", flex: 0.5 },
    {
      field: "name",
      headerName: "Student Name",
      cellClassName: "name-column--cell",
    },
    {
      field: "email",
      headerName: "Email",
      flex: 1,
    },
    {
      field: "phone_no",
      headerName: "Phone NO",
      flex: 1,
    },
    {
      field: "subject_name",
      headerName: "Subject Name",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "status",
      headerName: "Request Status",

      headerAlign: "left",
      align: "left",
    },
    {
      field: "goal_of_tutoring",
      headerName: "Goal",
      flex: 1,
    },
    {
      field: "total_hours",
      headerName: "Total Hours",
      flex: 1,
    },

    {
      field: "request_time",
      headerName: "Request Time",
      valueFormatter: (params) => {
        const requestTime = new Date(params.value); // Convert to Date object
        return format(requestTime, "yyyy-MM-dd HH:mm:ss"); // Format the date
      },
      flex: 1,
    },
    {
      field: "approve_time",
      headerName: "Approve Time",
      valueFormatter: (params) => {
        if (params.value) {
          const approveTime = new Date(params.value); // Convert to Date object
          return format(approveTime, "yyyy-MM-dd HH:mm:ss"); // Format the date
        } else {
          return ""; // Return an empty string for missing values
        }
      },
      flex: 1,
    },
  ];

  return (
    <Box m="20px">
      <Header
        title="CONTACTS"
        subtitle="List of Contacts for Future Reference"
      />
      <Box
        m="40px 0 0 0"
        height="75vh"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
          },
          "& .MuiDataGrid-cell": {
            borderBottom: "none",
          },
          "& .name-column--cell": {
            color: colors.greenAccent[300],
          },
          "& .MuiDataGrid-columnHeaders": {
            backgroundColor: colors.blueAccent[700],
            borderBottom: "none",
          },
          "& .MuiDataGrid-virtualScroller": {
            backgroundColor: colors.primary[400],
          },
          "& .MuiDataGrid-footerContainer": {
            borderTop: "none",
            backgroundColor: colors.blueAccent[700],
          },
          "& .MuiCheckbox-root": {
            color: `${colors.greenAccent[200]} !important`,
          },
          "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
            color: `${colors.grey[100]} !important`,
          },
        }}
      >
        <DataGrid
          rows={requests}
          columns={columns}
          components={{ Toolbar: GridToolbar }}
          pageSize={10}
          onRowClick={(row) => {
            navigate(`/requests/${row.id}`);
          }}
        />
      </Box>
    </Box>
  );
};

export default Reqeusts;
