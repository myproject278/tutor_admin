import React, { useContext, useEffect, useState } from "react";
import { Box, IconButton, useTheme } from "@mui/material";
import Badge from "@mui/material/Badge";
import LightModeOutlinedIcon from "@mui/icons-material/LightModeOutlined";
import DarkModeOutlinedIcon from "@mui/icons-material/DarkModeOutlined";
import NotificationsOutlinedIcon from "@mui/icons-material/NotificationsOutlined";
import SettingsOutlinedIcon from "@mui/icons-material/SettingsOutlined";
import PersonOutlinedIcon from "@mui/icons-material/PersonOutlined";
import SearchIcon from "@mui/icons-material/Search";
import { useNavigate } from "react-router-dom";
import Notification from "../../components/notifications";
import { useDispatch, useSelector } from "react-redux";
import { get_unread_notification_count } from "../../actions/notification_action";
import { ColorModeContext, tokens } from "../../theme";
import InputBase from "@mui/material/InputBase";

const Topbar = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const colorMode = useContext(ColorModeContext);
  const navigate = useNavigate();
  const [openModal, setOpenModal] = useState(false);
  const { authenticate, loading } = useSelector((state) => state.authReducer);

  const dispatch = useDispatch();
  useEffect(() => {
    if (authenticate) dispatch(get_unread_notification_count());
  }, [dispatch, authenticate]);

  const unread_count = useSelector(
    (state) => state.notificationReducer.unread_count
  );

  return (
    <Box display="flex" justifyContent="space-between" p={2}>
      {/* SEARCH BAR */}
      <Box
        display="flex"
        backgroundColor={colors.primary[400]}
        borderRadius="3px"
      >
        <InputBase sx={{ ml: 2, flex: 1 }} placeholder="Search" />
        <IconButton type="button" sx={{ p: 1 }}>
          <SearchIcon />
        </IconButton>
      </Box>

      {/* ICONS */}
      <Box display="flex">
        <IconButton onClick={colorMode.toggleColorMode}>
          {theme.palette.mode === "dark" ? (
            <DarkModeOutlinedIcon />
          ) : (
            <LightModeOutlinedIcon />
          )}
        </IconButton>
        <IconButton onClick={() => setOpenModal(true)}>
          <Badge badgeContent={unread_count} color="error">
            <NotificationsOutlinedIcon />
          </Badge>
        </IconButton>
        <IconButton>
          <SettingsOutlinedIcon />
        </IconButton>
        <IconButton onClick={() => navigate("/profile")}>
          <PersonOutlinedIcon />
        </IconButton>
      </Box>
      <Notification openModal={openModal} setOpenModal={setOpenModal} />
    </Box>
  );
};

export default Topbar;
