import React, { useState } from "react";
import {
  Button,
  IconButton,
  Modal,
  TextField,
  MenuItem,
  Select,
  Box,
} from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { useNavigate } from "react-router-dom";

const LevelOptions = ["Beginner", "Intermediate", "Advanced"];

const styles = {
  modalContainer: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    boxShadow: 24,
    p: 4,
  },
  header: {
    marginBottom: "1rem",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  actions: {
    marginTop: "1rem",
    display: "flex",
    justifyContent: "space-between",
  },
  editButton: {
    marginLeft: "auto",
    color: "#1976d2",
  },
  deleteButton: {
    color: "#e53935",
  },
};

const HeaderInfo = ({
  isModalOpen,
  setIsModalOpen,
  headerInfo,
  setHeaderInfo,
}) => {
  const [isEditing, setIsEditing] = useState(false);
  const navigate = useNavigate();
  const handleOpenModal = () => {
    setIsModalOpen(true);
  };
  console.log(headerInfo);
  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  const handleDelete = () => {
    // Implement delete functionality here
  };

  const handleSubmit = () => {
    // Implement submit functionality here
    setIsEditing(false);
  };

  return (
    <div>
      <Modal open={isModalOpen} onClose={handleCloseModal}>
        <Box sx={styles.modalContainer}>
          <div style={styles.header}>
            <h2>Info</h2>
          </div>

          <div>
            <div>
              <strong>Hedaer:</strong> {headerInfo.header_info}
            </div>
            <div>
              <strong>Hourly Rate:</strong> {headerInfo.hourly_rate}
            </div>
            <div>
              <strong>Level of Knowledge:</strong>{" "}
              {headerInfo.level_of_knowledge}
            </div>
            <div>
              <strong>Name:</strong> {headerInfo.name}
            </div>
            <div>
              <strong>Other Info:</strong> {headerInfo.other_info}
            </div>
            <div style={styles.actions}>
              <Button
                onClick={() =>
                  navigate(
                    `/subject_form/${headerInfo.id}?subject_name=${headerInfo.name}`
                  )
                }
                style={styles.editButton}
              >
                Edit
              </Button>
              <IconButton onClick={handleDelete}>
                <DeleteIcon style={styles.deleteButton} />
              </IconButton>
            </div>
          </div>
        </Box>
      </Modal>
    </div>
  );
};

export default HeaderInfo;
