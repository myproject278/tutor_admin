import { Box, Button } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import { mockDataContacts } from "../../data/mockData";
import Header from "../../components/Header";
import { useTheme } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { get_request_by_teacher_id } from "../../actions/request_action";
import { useNavigate } from "react-router-dom";
import { format, set } from "date-fns";
import { get_subjects_for_teacher } from "../../actions/subject_teacher_action";
import EditIcon from "@mui/icons-material/Edit";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import HeaderInfo from "./MySubjectModel";
const Subjects = () => {
  const [headerInfo, setHeaderInfo] = useState({
    hourlyRate: "",
    levelOfKnowledge: "",
    name: "",
    otherInfo: "",
  });
  const subject_list = useSelector(
    (state) => state.subjectTeacherReducer.teacher_subject_list
  );
  const [isModalOpen, setIsModalOpen] = useState(false);

  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(get_subjects_for_teacher());
  }, []);
  const columns = [
    { field: "id", headerName: "ID" },
    {
      field: "name",
      headerName: "Subject Name",
      cellClassName: "name-column--cell",
      flex: 0.7,
    },
    {
      field: "hourly_rate",
      headerName: "Hourly Rate",
    },
    {
      field: "level_of_knowledge",
      headerName: "Expertise Level",
    },
    {
      field: "header_info",
      headerName: "Heading",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "other_info",
      headerName: "Extra",
      flex: 1,
      headerAlign: "left",
      align: "left",
    },
  ];

  return (
    <Box m="20px">
      <Header title="Subjects" subtitle="List of Subjects of You Teaching" />
      <Box
        m="40px 0 0 0"
        height="75vh"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
          },
          "& .MuiDataGrid-cell": {
            borderBottom: "none",
          },
          "& .name-column--cell": {
            color: colors.greenAccent[300],
          },
          "& .MuiDataGrid-columnHeaders": {
            backgroundColor: colors.blueAccent[700],
            borderBottom: "none",
          },
          "& .MuiDataGrid-virtualScroller": {
            backgroundColor: colors.primary[400],
          },
          "& .MuiDataGrid-footerContainer": {
            borderTop: "none",
            backgroundColor: colors.blueAccent[700],
          },
          "& .MuiCheckbox-root": {
            color: `${colors.greenAccent[200]} !important`,
          },
          "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
            color: `${colors.grey[100]} !important`,
          },
        }}
      >
        <DataGrid
          rows={subject_list}
          columns={columns}
          components={{ Toolbar: GridToolbar }}
          pageSize={10}
          onCellClick={(row) => {
            setHeaderInfo(row.row);
            setIsModalOpen(true);
          }}
        />
      </Box>
      <HeaderInfo
        isModalOpen={isModalOpen}
        setIsModalOpen={setIsModalOpen}
        headerInfo={headerInfo}
        setHeaderInfo={setHeaderInfo}
      />
    </Box>
  );
};

export default Subjects;
