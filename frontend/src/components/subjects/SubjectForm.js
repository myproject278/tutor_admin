import React, { useEffect, useState } from "react";
import { Button, TextField, MenuItem, Select, Box, Grid } from "@mui/material";
import { sub } from "date-fns";
import Header from "../Header";
import { useDispatch, useSelector } from "react-redux";
import { getSubjectsForDropdown } from "../../actions/subject_action";
import { useSnackbar } from "notistack"; // Import inside the component
import { useLocation, useNavigate, useParams } from "react-router-dom";
import {
  add_subjects_for_teacher,
  get_subject_info_for_update,
  update_subjects_for_teacher,
} from "../../actions/subject_teacher_action";

const LevelOptions = ["Beginner", "Intermediate", "Advanced"];

const SubjectForm = () => {
  const intial_state = {
    hourly_rate: "",
    level_of_knowledge: "Select Experties",
    subject_id: "0",
    other_info: "",
    header_info: "",
  };
  const { subject_id } = useParams();
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const subject_name = searchParams.get("subject_name");

  const subject_details = useSelector(
    (state) => state.subjectTeacherReducer.subject_details
  );

  const [subject, setSubject] = useState(intial_state);
  const subject_dropdown = useSelector(
    (state) => state.subjectReducer.subjects_dropdown
  );
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getSubjectsForDropdown());
  }, []);
  useEffect(() => {
    if (subject_id && subject_details) {
      setSubject({
        ...subject_details,
        level_of_knowledge:
          LevelOptions[subject_details.level_of_knowledge - 1],
        subject_id: subject_id,
      });
    }
  }, [subject_details]);
  useEffect(() => {
    if (subject_id) dispatch(get_subject_info_for_update(subject_id));
  }, [subject_id]);
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setSubject((prevSubject) => ({
      ...prevSubject,
      [name]: value,
    }));
  };

  const handleLevelChange = (e) => {
    setSubject((prevSubject) => ({
      ...prevSubject,
      level_of_knowledge: e.target.value,
    }));
  };
  const handleSubjectChange = (e) => {
    setSubject((prevSubject) => ({
      ...prevSubject,
      subject_id: e.target.value,
    }));
  };
  const { enqueueSnackbar } = useSnackbar(); // Initialize the useSnackbar hook
  const navigate = useNavigate();
  const submit_hadeler = () => {
    if (subject.level_of_knowledge === "Select Experties") {
      enqueueSnackbar("Please select an expertise level", { variant: "error" });
    } else if (subject.subject_id === "0") {
      enqueueSnackbar("Please select a subject", { variant: "error" });
    } else {
      if (subject_id)
        dispatch(
          update_subjects_for_teacher(
            {
              ...subject,
              level_of_knowledge:
                subject.level_of_knowledge == "Beginner"
                  ? 1
                  : subject.level_of_knowledge == "Intermediate"
                  ? 2
                  : 3,
            },
            navigate
          )
        );
      else
        dispatch(
          add_subjects_for_teacher(
            {
              ...subject,
              level_of_knowledge:
                subject.level_of_knowledge == "Beginner"
                  ? 1
                  : subject.level_of_knowledge == "Intermediate"
                  ? 2
                  : 3,
            },
            navigate
          )
        );
    }
  };
  const reset_input = () => {
    if (subject_id) {
      setSubject({
        ...subject_details,
        level_of_knowledge:
          LevelOptions[subject_details.level_of_knowledge - 1],
        subject_id: subject_id,
      });
    } else setSubject(intial_state);
  };

  return (
    <Box p={2}>
      <Header title="Subject Form" subtitle="Select Subject For Tutoring" />

      <TextField
        name="header_info"
        label="Header"
        value={subject.header_info}
        onChange={handleInputChange}
        fullWidth
        margin="normal"
        variant="outlined"
      />
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <TextField
            type="number"
            name="hourly_rate"
            label="Hourly Rate"
            value={subject.hourly_rate}
            onChange={handleInputChange}
            fullWidth
            margin="normal"
            variant="outlined"
          />
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Select
            name="level_of_knowledge"
            label="Expertise Level"
            value={subject.level_of_knowledge}
            onChange={handleLevelChange}
            fullWidth
            margin="normal"
            variant="outlined"
            defaultValue="expertise_level"
          >
            <MenuItem value="Select Experties" disabled>
              Select Experties
            </MenuItem>
            {LevelOptions.map((option) => (
              <MenuItem key={option} value={option}>
                {option}
              </MenuItem>
            ))}
          </Select>
        </Grid>
        <Grid item xs={6}>
          <Select
            name="level_of_knowledge"
            label="Expertise Level"
            value={subject.subject_id}
            onChange={handleSubjectChange}
            fullWidth
            margin="normal"
            variant="outlined"
            defaultValue="expertise_level"
          >
            {subject_id ? (
              <MenuItem value={subject_id}>{subject_name}</MenuItem>
            ) : (
              <MenuItem value="0" disabled>
                Select Subjects
              </MenuItem>
            )}
            {subject_dropdown.map((sub) => (
              <MenuItem key={sub.id} value={sub.id} disabled>
                {sub.name}
              </MenuItem>
            ))}
          </Select>
        </Grid>
      </Grid>

      <TextField
        name="other_info"
        label="Other Info"
        value={subject.other_info}
        onChange={handleInputChange}
        fullWidth
        margin="normal"
        variant="outlined"
        multiline
        rows={4}
      />
      <Box mt={2}>
        <Button variant="contained" color="primary" onClick={submit_hadeler}>
          Submit
        </Button>
        <Button
          variant="outlined"
          color="primary"
          style={{ marginLeft: 8 }}
          onClick={reset_input}
        >
          Reset
        </Button>
      </Box>
    </Box>
  );
};

export default SubjectForm;
