import React, { useState } from "react";
import {
  Checkbox,
  FormControlLabel,
  Button,
  Grid,
  Paper,
  Typography,
} from "@mui/material";

const daysOfWeek = [
  "sunday",
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday",
  "saturday",
];

const AvailabilityForm = ({ available_days, SetTeacher, teacher }) => {
  const [availability, setAvailability] = useState({
    sunday: available_days.sunday,
    monday: available_days.monday,
    tuesday: available_days.thursday,
    wednesday: available_days.wednesday,
    thursday: available_days.thursday,
    friday: available_days.friday,
    saturday: available_days.saturday,
  });

  const handleCheckboxChange = (day, hour) => {
    const updatedAvailability = { ...availability };
    updatedAvailability[day][hour] =
      updatedAvailability[day][hour] == 0 ? 1 : 0;
    setAvailability(updatedAvailability);
    SetTeacher({ ...teacher, available_slots: updatedAvailability });
  };

  const getAmPmLabel = (hour) => {
    if (hour === 0) return "12 AM";
    if (hour < 12) return `${hour} AM`;
    if (hour === 12) return "12 PM";
    return `${hour - 12} PM`;
  };

  return (
    <Paper
      elevation={3}
      style={{ padding: "16px", background: "inherit", margin: "5px" }}
    >
      <form>
        <Grid container spacing={2}>
          {daysOfWeek.map((day) => (
            <Grid key={day} item xs={12}>
              <Typography style={{ color: "black" }}>
                {day.charAt(0).toUpperCase() + day.slice(1)}:
              </Typography>
              <Grid container spacing={1}>
                {availability[day].map((hour, index) => (
                  <Grid key={index} item xs={2}>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={hour != 0}
                          onChange={() => handleCheckboxChange(day, index)}
                        />
                      }
                      label={getAmPmLabel(index)}
                      style={{ color: "black" }}
                    />
                  </Grid>
                ))}
              </Grid>
              <hr />
            </Grid>
          ))}
        </Grid>
      </form>
    </Paper>
  );
};

export default AvailabilityForm;
