import React, { useEffect, useState } from "react";
import CancelIcon from "@mui/icons-material/Cancel";
import { useDispatch, useSelector } from "react-redux";
import {
  get_all_teacher_info,
  update_teacher,
} from "../../actions/auth_action";
import { get_image_url } from "../../helper/util";
import EditIcon from "@material-ui/icons/Edit"; // Import EditIcon
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";

import { makeStyles } from "@material-ui/core/styles";
import {
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Grid,
  Button,
  Typography,
  TextField,
} from "@material-ui/core";
import AvailabilityForm from "./AvailabilityInput";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 1100,
    margin: "auto",
    marginTop: theme.spacing(3),
  },
  media: {
    height: 200,
  },
  avatar: {
    width: theme.spacing(10),
    height: theme.spacing(10),
    margin: "auto",
  },
  aboutCard: {
    backgroundColor: "#f8f9fa",
    padding: theme.spacing(2),
    marginBottom: theme.spacing(5),
  },
  statContainer: {
    display: "flex",
    justifyContent: "end",
    padding: theme.spacing(1),
  },
  statItem: {
    flex: 1,
    textAlign: "center",
  },
  photoGrid: {
    marginBottom: theme.spacing(4),
  },
  photoItem: {
    marginBottom: theme.spacing(2),
  },
}));

const get_time_am_pm = (i) => {
  if (i % 12 == 0) return `12 ${i == 0 ? "Pm" : "Am"}`;
  else return `${i % 12} ${i / 12 > 0 ? "Pm" : "Am"}`;
};
const time_from_arr = (arr = []) => {
  const res = [];
  let flag = false;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] != 0 && flag == false) {
      flag = true;
      res.push(get_time_am_pm(i));
    } else if (arr[i] == 0 && flag == true) {
      res[res.length - 1] = res[res.length - 1] + " - " + get_time_am_pm(i);
      flag = false;
    }
  }
  if (flag) res[res.length - 1] = res[res.length - 1] + " - 12 Am";
  return res;
};

export default function UserProfile() {
  const [editMode, setEditMode] = useState(false);
  const [new_teacher, setNewTeacher] = useState({});
  const classes = useStyles();
  const dispatch = useDispatch();
  const temp_teacher = useSelector((state) => state.authReducer.user);
  const [teacher, SetTeacher] = useState({});
  useEffect(() => {
    SetTeacher(temp_teacher);
  }, [temp_teacher]);
  useEffect(() => {
    if (teacher && teacher.id) dispatch(get_all_teacher_info(teacher.id));
  }, [teacher?.id]);
  let available_days = {};
  if (teacher && teacher.available_slots)
    available_days = JSON.parse(teacher?.available_slots);
  const handleEdit = () => {
    setNewTeacher({ ...teacher, available_slots: available_days });
    setSelectedLanguages(JSON.parse(teacher?.speaking_languaes));

    setEditMode(true);
  };
  const handleSave = () => {
    dispatch(
      update_teacher({
        ...new_teacher,
        speaking_languaes: JSON.stringify(selectedLanguages),
        available_slots: JSON.stringify(new_teacher.available_slots),
        sunday: new_teacher.available_slots["sunday"].includes(1) ? 1 : 0,
        monday: new_teacher.available_slots["monday"].includes(1) ? 1 : 0,
        tuesday: new_teacher.available_slots["tuesday"].includes(1) ? 1 : 0,
        wednesday: new_teacher.available_slots["wednesday"].includes(1) ? 1 : 0,
        thursday: new_teacher.available_slots["thursday"].includes(1) ? 1 : 0,
        friday: new_teacher.available_slots["friday"].includes(1) ? 1 : 0,
        saturday: new_teacher.available_slots["saturday"].includes(1) ? 1 : 0,
      })
    );
    setEditMode(false);
  };
  const languages = [
    "English",
    "Marathi",
    "Hindi",
    "Spanish",
    "French",
    "German",
  ];
  const [selectedLanguages, setSelectedLanguages] = useState([]);

  const handleLanguageChange = (event) => {
    setSelectedLanguages(event.target.value);
  };
  const changeHandler = (e) => {
    setNewTeacher({ ...new_teacher, [e.target.name]: e.target.value });
  };
  return (
    <div className={classes.root}>
      <Card>
        <CardHeader
          avatar={
            <div className={classes.avatar}>
              <CardMedia
                component="img"
                src={get_image_url(teacher?.image_url)}
                alt="User Avatar"
              />
            </div>
          }
          title={
            editMode ? (
              <TextField
                value={new_teacher?.name}
                placeholder="Enter Name"
                name="name"
                onChange={changeHandler}
              />
            ) : (
              teacher?.name
            )
          }
          subheader={
            editMode ? (
              <TextField
                value={new_teacher?.current_position}
                placeholder="Enter Position"
                name="current_position"
                fullWidth
                onChange={changeHandler}
              />
            ) : (
              teacher?.current_position
            )
          }
          action={
            editMode ? (
              <div>
                <Button
                  onClick={handleSave}
                  variant="contained"
                  color="primary"
                >
                  Save
                </Button>
                <Button
                  onClick={() => {
                    setEditMode(false);
                    setNewTeacher(teacher);
                  }}
                  color="secondary"
                >
                  <CancelIcon />
                </Button>
              </div>
            ) : (
              <EditIcon onClick={handleEdit} />
            )
          }
        />
        <CardContent>
          <div className={classes.statContainer}>
            <div className={classes.statItem}>
              <Typography variant="h5" component="p">
                {editMode ? (
                  <TextField
                    value={new_teacher?.email}
                    placeholder="Enter email"
                    name="email"
                    onChange={changeHandler}
                  />
                ) : (
                  teacher?.email
                )}
              </Typography>
              <Typography variant="body2" color="textSecondary">
                Email
              </Typography>
            </div>
            <div className={classes.statItem}>
              <Typography variant="h5" component="p">
                {editMode ? (
                  <TextField
                    value={new_teacher?.phone_no}
                    placeholder="Phone No"
                    name="phone_no"
                    onChange={changeHandler}
                  />
                ) : (
                  teacher?.phone_no
                )}
              </Typography>
              <Typography variant="body2" color="textSecondary">
                Phone NO
              </Typography>
            </div>
          </div>
          <div className={classes.aboutCard}>
            <Typography variant="h6">Speaking Language</Typography>
            <div className={classes.aboutCard}>
              {editMode ? (
                <FormControl fullWidth>
                  <InputLabel style={{ color: "blue" }}>
                    Select Languages
                  </InputLabel>
                  <Select
                    multiple
                    value={selectedLanguages}
                    onChange={handleLanguageChange}
                    renderValue={(selected) => selected.join(", ")}
                    style={{ color: "black" }}
                  >
                    {languages.map((language) => (
                      <MenuItem key={language} value={language}>
                        {language}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              ) : (
                teacher?.speaking_languaes &&
                JSON.parse(teacher?.speaking_languaes).map((item, index) => (
                  <Typography
                    variant="body2"
                    className="font-italic"
                    key={index}
                  >
                    {item}
                  </Typography>
                ))
              )}
            </div>
            <Typography variant="h6">Bio</Typography>
            <div className={classes.aboutCard}>
              {editMode ? (
                <TextField
                  value={new_teacher?.bio}
                  placeholder="Bio"
                  name="bio"
                  fullWidth
                  multiline
                  onChange={changeHandler}
                  rows={3}
                />
              ) : (
                teacher?.bio
              )}
            </div>

            <Typography variant="h6">Secound Last Education </Typography>
            <div className={classes.aboutCard}>
              {editMode ? (
                <TextField
                  value={new_teacher?.secound_last_education}
                  placeholder="Secound Last Education"
                  fullWidth
                  onChange={changeHandler}
                  name="secound_last_education"
                />
              ) : (
                teacher?.secound_last_education
              )}
            </div>
            <Typography variant="h6"> Last Education </Typography>
            <div className={classes.aboutCard}>
              {editMode ? (
                <TextField
                  value={new_teacher?.last_education}
                  placeholder="Last Education"
                  fullWidth
                  onChange={changeHandler}
                  name="last_education"
                />
              ) : (
                teacher?.last_education
              )}
            </div>
          </div>

          <div className={classes.photoGrid}>
            <Typography variant="h6">Available Time : </Typography>
            <Grid container spacing={2}>
              {editMode ? (
                <AvailabilityForm
                  available_days={available_days}
                  SetTeacher={setNewTeacher}
                  teacher={new_teacher}
                />
              ) : (
                <div style={{ margin: "25px", width: "100%" }}>
                  <div
                    className="availabilityslotdiv"
                    style={{ color: "#6a6969" }}
                  >
                    <p>
                      Sunday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.sunday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <hr />
                    <p>
                      Monday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {" "}
                        {time_from_arr(available_days?.monday)?.join(" , ")}
                      </Typography>
                    </p>
                    <hr />
                    <p>
                      Tuesday :
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.tuesday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <hr />
                    <p>
                      Wednesday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.wednesday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <hr />
                    <p>
                      Thursday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.thursday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <hr />
                    <p>
                      Friday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.friday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <hr />
                    <p>
                      Saturday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.saturday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <hr />
                  </div>
                </div>
              )}
            </Grid>
          </div>
        </CardContent>
      </Card>
    </div>
  );
}
