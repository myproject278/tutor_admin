import React from "react";
import { get_image_url } from "../../helper/util";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // Import the carousel styles
import { Paper } from "@material-ui/core";

const ImageCard = ({ image_urls }) => {
  let images = [];

  if (image_urls && image_urls.length > 0) {
    images = JSON.parse(image_urls);
  }

  return (
    <Carousel showArrows={true} showThumbs={false}>
      {images.map(({ img }, index) => (
        <div key={index}>
          <img
            className="d-block w-100"
            src={get_image_url(img)}
            alt={`Slide ${index}`}
          />
        </div>
      ))}
    </Carousel>
  );
};

export default ImageCard;
