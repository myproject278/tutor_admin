import React, { useEffect, useState } from "react";
import {
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Button,
  Typography,
  ButtonBase,
} from "@material-ui/core/";
import ThumbUpAltIcon from "@material-ui/icons/ThumbUpAlt";
import ThumbUpAltOutlined from "@material-ui/icons/ThumbUpAltOutlined";
import DeleteIcon from "@material-ui/icons/Delete";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import style from "./single_post_style";
import { useNavigate } from "react-router-dom";
import { get_image_url } from "../../../helper/util";
//title:,message,creater,tags,selectedFile,likeCount createdAt:
const Post = ({ post }) => {
  const auth = useSelector((state) => state.auth);
  const classes = style();
  const [likes, setLikes] = useState(post?.likes);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const updateHandler = () => {
    navigate(`/post_form/${post.id}`);
  };

  const openPost = () => {
    navigate(`/posts/${post.id}`);
  };
  let image = null;
  if (post && post.image_urls && post.image_urls.length > 3)
    image = JSON.parse(post.image_urls)[0].img;
  let tags = [];
  if (post && post.tags) tags = JSON.parse(post.tags);
  return (
    <Card className={classes.card} raised elevation={6}>
      <ButtonBase onClick={openPost} className={classes.cardAction}>
        <CardMedia className={classes.media} image={get_image_url(image)} />

        <div className={classes.overlay}>
          <Typography variant="body2">{moment(post.date).fromNow()}</Typography>
        </div>
      </ButtonBase>

      <Button className={classes.overlay2} onClick={updateHandler}>
        <MoreHorizIcon />
      </Button>

      <ButtonBase onClick={openPost} className={classes.cardAction}>
        <div className={classes.details}>
          <Typography color="textSecondary" variant="body2">
            {tags.map((tag) => `#${tag} `)}
          </Typography>
        </div>
        <Typography className={classes.title} color="textPrimary" variant="h5">
          {post.header}
        </Typography>
        <CardContent>
          <Typography>{post.description}</Typography>
        </CardContent>
      </ButtonBase>
    </Card>
  );
};

export default Post;
