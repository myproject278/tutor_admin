import React, { useEffect } from "react";
import style from "./post_style";
import { useDispatch, useSelector } from "react-redux";
import { CircularProgress, Grid } from "@material-ui/core";
import { get_posts_for_teacher } from "../../../actions/post_action";
import Post from "./SinglePost";
import Header from "../../Header";

const Posts = ({ setUpdate }) => {
  const styleObj = style();
  const posts = useSelector((state) => state.postReducer.posts);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(get_posts_for_teacher());
  }, []);
  console.log(posts);
  if (!posts.length && posts.isLoading) return "No Post Present";
  return (
    <div style={{ padding: "20px 40px" }}>
      <Header title="Posts" subtitle="Your Posts" />
      {posts.isLoading ? (
        <CircularProgress />
      ) : (
        <Grid
          className={styleObj.container}
          container
          spacing={3}
          style={{ width: "100%" }}
        >
          {posts.map((post) => (
            <Grid key={post._id} item xs={12} sm={12} md={6} lg={3}>
              <Post post={post} />
            </Grid>
          ))}
        </Grid>
      )}
    </div>
  );
};

export default Posts;
