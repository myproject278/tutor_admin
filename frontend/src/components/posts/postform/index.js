import React, { useEffect, useState } from "react";
import { Button, TextField, MenuItem, Select, Box, Grid } from "@mui/material";
import { sub } from "date-fns";
import Header from "../../Header";
import { useDispatch, useSelector } from "react-redux";
import { useSnackbar } from "notistack"; // Import inside the component
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { Stack } from "@mui/material";
import { AddPhotoAlternate } from "@mui/icons-material";
import ChipInput from "material-ui-chip-input";
import {
  add_post,
  get_post_for_update,
  update_post,
} from "../../../actions/post_action";

const LevelOptions = ["Beginner", "Intermediate", "Advanced"];

const SubjectForm = () => {
  const { post_id } = useParams();
  const [images, setImages] = useState([]);
  const [header, setHeader] = useState("");
  const [description, setDescription] = useState("");
  const [tags, setTages] = useState([]);
  const dispatch = useDispatch();
  const handleImageChange = (event) => {
    const selectedImages = event.target.files[0];
    setImages([...images, selectedImages]);
  };

  const post_details = useSelector((state) => state.postReducer.post_details);
  console.log(post_details);

  useEffect(() => {
    if (post_id && post_details) {
      setHeader(post_details.header);
      setDescription(post_details.description);
      if (post_details.tags) setTages(JSON.parse(post_details.tags));
    }
  }, [post_details]);
  useEffect(() => {
    if (post_id) dispatch(get_post_for_update(post_id));
  }, [post_id]);

  const addTag = (tag) => {
    setTages([...tags, tag]);
  };
  const deleteTag = (tag) => {
    setTages(tags.filter((t) => t != tag));
  };

  const { enqueueSnackbar } = useSnackbar(); // Initialize the useSnackbar hook
  const navigate = useNavigate();
  const submit_hadeler = () => {
    if (header == "") {
      enqueueSnackbar("Please add header ", { variant: "error" });
    } else {
      if (post_id)
        dispatch(
          update_post(
            {
              id: post_details.id,
              header,
              description,
              tags: JSON.stringify(tags),
            },
            navigate
          )
        );
      else {
        const form = new FormData();
        form.set("header", header);
        form.set("description", description);
        form.set("tags", JSON.stringify(tags));
        images.forEach((file, index) => {
          form.append("images", file);
        });
        console.log(images);
        dispatch(add_post(form, navigate));
      }
    }
  };
  const reset_input = () => {
    // if (subject_id) {
    //   setPost({
    //     ...subject_details,
    //     level_of_knowledge:
    //       LevelOptions[subject_details.level_of_knowledge - 1],
    //     subject_id: subject_id,
    //   });
    // } else
  };

  return (
    <Box p={2}>
      <Header title="Post Form" subtitle="add / update posts" />

      <TextField
        name="header"
        label="Header"
        value={header}
        onChange={(e) => setHeader(e.target.value)}
        fullWidth
        margin="normal"
        variant="outlined"
      />

      <ChipInput
        style={{ width: "50%" }}
        value={tags}
        label="Tags"
        variant="outlined"
        onAdd={addTag}
        onDelete={deleteTag}
      />

      <TextField
        name="description"
        label="Description"
        value={description}
        onChange={(e) => setDescription(e.target.value)}
        fullWidth
        margin="normal"
        variant="outlined"
        multiline
        rows={4}
      />
      {!post_id && (
        <Grid>
          <input
            type="file"
            accept="image/*"
            multiple
            style={{ display: "none" }}
            id="image-input"
            onChange={handleImageChange}
          />
          <label htmlFor="image-input">
            <Button
              variant="outlined"
              color="primary"
              startIcon={<AddPhotoAlternate />}
              component="span"
            >
              Add Images
            </Button>
          </label>
          <Stack direction="column" spacing={2}>
            {images.map((image, index) => (
              <div key={index}>{image.name}</div>
            ))}
          </Stack>
        </Grid>
      )}
      <Box mt={2}>
        <Button variant="contained" color="primary" onClick={submit_hadeler}>
          Submit
        </Button>
        <Button
          variant="outlined"
          color="primary"
          style={{ marginLeft: 8 }}
          onClick={reset_input}
        >
          Reset
        </Button>
      </Box>
    </Box>
  );
};

export default SubjectForm;
