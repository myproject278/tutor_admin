import React, { useState, useEffect } from "react";
import {
  Box,
  Button,
  TextField,
  Grid,
  Typography,
  IconButton,
  InputAdornment,
} from "@mui/material";
import { Formik } from "formik";
import * as yup from "yup";
import useMediaQuery from "@mui/material/useMediaQuery";
import Header from "../../components/Header";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import { useSnackbar } from "notistack";
import { useDispatch, useSelector } from "react-redux";
import { signIn, signUp } from "../../actions/auth_action";
import { useLocation, useNavigate } from "react-router-dom";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";

const Form = () => {
  const isNonMobile = useMediaQuery("(min-width:600px)");

  const handleFormSubmit = (values) => {
    if (!isSignIn) {
      dispatch(signIn({ email: values.email, password: values.password }));
    } else {
      if (values.password !== values.confirm_password) {
        setCheckPassword(true);
      } else {
        const form = new FormData();
        form.set("name", `${values.firstName} ${values.lastName}`);
        form.set("mobile_no", values.contact);
        form.set("email", values.email);
        form.set("password", values.password);

        form.set("images", image);
        dispatch(signUp(form));
      }
    }
  };
  const [isSignIn, setIsSignIn] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [checkPasswod, setCheckPassword] = useState(false);
  const [image, setImage] = useState();

  const navigate = useNavigate();
  const location = useLocation();
  const { enqueueSnackbar } = useSnackbar();
  const { authenticate, loading, error, path } = useSelector(
    (state) => state.authReducer
  );
  const dispatch = useDispatch();

  const auth = useSelector((state) => state.authReducer);

  useEffect(() => {
    if (error) {
      enqueueSnackbar(error, { variant: "error" });
    }
    if (authenticate) {
      if (path) navigate(path);
      else navigate("/");
    }
  }, [authenticate, enqueueSnackbar]);
  const handleImageChange = (e) => {
    const file = e.target.files[0];
    setImage(file);
  };

  const handleShowPassword = () => {
    setShowPassword((prev) => !prev);
  };

  const switchMod = () => {
    setIsSignIn((prev) => !prev);
    console.log(isSignIn);
  };

  const checkoutSchema = yup.object().shape(
    isSignIn
      ? {
          firstName: yup.string().required("required"),
          lastName: yup.string().required("required"),
          email: yup.string().email("invalid email").required("required"),
          contact: yup
            .string()
            .matches(phoneRegExp, "Phone number is not valid")
            .required("required"),
          password: yup.string().required("required"),
          confirm_password: yup.string().required("required"),
        }
      : {
          email: yup.string().email("invalid email").required("required"),

          password: yup.string().required("required"),
        }
  );
  return (
    <Box m="20px">
      <Header title="CREATE USER" subtitle="Create a New User Profile" />

      <Formik
        onSubmit={handleFormSubmit}
        initialValues={initialValues}
        validationSchema={checkoutSchema}
      >
        {({
          values,
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
        }) => (
          <form onSubmit={handleSubmit}>
            <Box
              display="grid"
              gap="30px"
              gridTemplateColumns="repeat(4, minmax(0, 1fr))"
              sx={{
                "& > div": { gridColumn: isNonMobile ? undefined : "span 4" },
              }}
            >
              {isSignIn && (
                <TextField
                  fullWidth
                  variant="filled"
                  type="text"
                  label="First Name"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.firstName}
                  name="firstName"
                  error={!!touched.firstName && !!errors.firstName}
                  helperText={touched.firstName && errors.firstName}
                  sx={{ gridColumn: "span 2" }}
                />
              )}
              {isSignIn && (
                <TextField
                  fullWidth
                  variant="filled"
                  type="text"
                  label="Last Name"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.lastName}
                  name="lastName"
                  error={!!touched.lastName && !!errors.lastName}
                  helperText={touched.lastName && errors.lastName}
                  sx={{ gridColumn: "span 2" }}
                />
              )}
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Email"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.email}
                name="email"
                error={!!touched.email && !!errors.email}
                helperText={touched.email && errors.email}
                sx={{ gridColumn: "span 4" }}
              />
              {isSignIn && (
                <TextField
                  fullWidth
                  variant="filled"
                  type="text"
                  label="Contact Number"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.contact}
                  name="contact"
                  error={!!touched.contact && !!errors.contact}
                  helperText={touched.contact && errors.contact}
                  sx={{ gridColumn: "span 4" }}
                />
              )}
              <TextField
                fullWidth
                variant="filled"
                type={showPassword ? "text" : "password"}
                label="Password"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.password}
                name="password"
                error={!!touched.password && !!errors.password}
                helperText={touched.password && errors.password}
                sx={{ gridColumn: "span 4" }}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton onClick={handleShowPassword}>
                        {showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
              {isSignIn && (
                <TextField
                  fullWidth
                  variant="filled"
                  type="password"
                  label="Confirm Passoword"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.confirm_password}
                  name="address1"
                  error={
                    !!touched.confirm_password && !!errors.confirm_password
                  }
                  helperText={
                    touched.confirm_password && errors.confirm_password
                  }
                  sx={{ gridColumn: "span 4" }}
                />
              )}

              {isSignIn && (
                <div
                  style={{
                    width: "97%",
                    margin: "10px 12px",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-around",
                  }}
                >
                  <Typography align="center">Profile Pic</Typography>
                  <input
                    type="file"
                    accept="image/*"
                    onChange={handleImageChange}
                  />
                </div>
              )}
            </Box>
            <Box display="flex" justifyContent="end" mt="20px">
              <Button type="submit" color="secondary" variant="contained">
                {isSignIn ? "Create New User" : "Sign In"}
              </Button>
            </Box>
          </form>
        )}
      </Formik>
      <Box display="flex" justifyContent="end" mt="20px">
        <Button onClick={switchMod} color="secondary" variant="contained">
          {!isSignIn
            ? `Don't Have an Account? Sign Up`
            : `Already Have an Account? Sign In`}
        </Button>
      </Box>
    </Box>
  );
};

const phoneRegExp =
  /^((\+[1-9]{1,4}[ -]?)|(\([0-9]{2,3}\)[ -]?)|([0-9]{2,4})[ -]?)*?[0-9]{3,4}[ -]?[0-9]{3,4}$/;

const initialValues = {
  firstName: "",
  lastName: "",
  email: "",
  contact: "",
  address1: "",
  address2: "",
};

export default Form;
