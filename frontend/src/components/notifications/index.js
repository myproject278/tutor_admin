import { useDispatch, useSelector } from "react-redux";
import React, { useEffect } from "react";
import {
  Badge,
  Button,
  Modal,
  Typography,
  List,
  ListItem,
  ListItemText,
} from "@material-ui/core"; // Import from @material-ui/core
import {
  get_notifications,
  handle_notification_read,
} from "../../actions/notification_action";
import { makeStyles } from "@material-ui/core/styles"; // Import makeStyles from @material-ui/core/styles
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  modalContent: {
    backgroundColor: "white",
    color: "black",
    padding: "20px",
    maxWidth: "400px",
    margin: "0 auto",
    borderRadius: "10px",
    outline: "none",
    boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
  },
  unreadItem: {
    borderLeft: "3px solid #ff9800",
    margin: "10px 0",
    padding: "10px",
    backgroundColor: "#f7f7f7",
    borderRadius: "5px",
    display: "flex",
    alignItems: "center",
    "&:hover": {
      backgroundColor: "#fff",
    },
  },
  readItem: {
    borderLeft: "3px solid #4caf50",
    margin: "10px 0",
    padding: "10px",
    backgroundColor: "#f0f0f0",
    borderRadius: "5px",
    display: "flex",
    alignItems: "center",
    "&:hover": {
      backgroundColor: "#fff",
    },
  },
  listItemText: {
    flex: 1,
    marginLeft: "10px",
  },
}));

const NotificationComponent = ({ openModal, setOpenModal }) => {
  const classes = useStyles();

  const dispatch = useDispatch();
  useEffect(() => {
    if (openModal) dispatch(get_notifications());
  }, [dispatch, openModal]);

  const notificationList = useSelector(
    (state) => state.notificationReducer.notifications
  );

  function handleNotificationClick(id) {
    // Your code for handling notification clicks
  }

  const handleCloseModal = () => {
    setOpenModal(false);
  };
  const notification_click_handler = (is_read, notification_id) => {
    if (is_read == 0) dispatch(handle_notification_read({ notification_id }));
    setOpenModal(false);
  };

  return (
    <Modal
      open={openModal}
      onClose={handleCloseModal}
      aria-labelledby="notification-modal"
      style={{ marginTop: "20px" }}
    >
      <div className={classes.modalContent}>
        {notificationList.length > 0 ? (
          <List>
            {notificationList.map((notification) => (
              <ListItem
                key={notification.id}
                className={
                  notification.is_read ? classes.unreadItem : classes.readItem
                }
                onClick={() => handleNotificationClick(notification.id)}
              >
                <Badge
                  variant="dot"
                  color="secondary"
                  invisible={!notification.isUnread}
                >
                  <div className={classes.listItemText}>
                    <Typography variant="subtitle1">
                      {notification.message}
                    </Typography>
                    <Link
                      href={notification.redirect_link}
                      onClick={() =>
                        notification_click_handler(
                          notification.is_read,
                          notification.id
                        )
                      }
                    >
                      View Details
                    </Link>
                  </div>
                </Badge>
              </ListItem>
            ))}
          </List>
        ) : (
          <Typography>No new notifications.</Typography>
        )}
        <Button onClick={handleCloseModal}>Close</Button>
      </div>
    </Modal>
  );
};

export default NotificationComponent;
