import React, { useState } from "react";
import {
  Card,
  CardContent,
  Typography,
  Button,
  FormGroup,
  FormControlLabel,
  Checkbox,
} from "@mui/material";
import { formatDate } from "../../utils/functions";
import { fromUnixTime } from "date-fns";
import { useDispatch, useSelector } from "react-redux";
import { hanndle_approve_slot } from "../../actions/request_action";
import { send_email } from "../../actions/mail_action";

const ShowRequestSlot = ({
  slot_1,
  slot_2,
  slot_3,
  total_hours,
  request_id,
  student_email,
  subject_name,
}) => {
  const [selectedSlot, setSelectedSlot] = useState(null);
  const dispatch = useDispatch();
  const handleSlotChange = (slot) => {
    setSelectedSlot((prevSlot) => (prevSlot === slot ? null : slot));
  };
  const { user } = useSelector((state) => state.authReducer);

  const handleSubmit = async () => {
    try {
      const returndata = await hanndle_approve_slot(
        { approve_time: selectedSlot, total_hours },
        request_id
      );
      console.log(returndata);
      if (returndata.status == "success") {
        console.log(student_email);
        if (student_email) {
          const email_url = await send_email({
            recipient: student_email,
            subject: "New Tutoring Request",
            message: `${user?.name} approve request for teaching ${subject_name}`,
          });
          console.log(email_url);
          const newWindow = window.open(email_url);
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Card elevation={3} style={{ width: "400px", margin: "0 auto" }}>
      <CardContent>
        <Typography variant="h6" gutterBottom>
          Requested Time Slots
        </Typography>
        <FormGroup>
          {slot_1 && (
            <FormControlLabel
              control={
                <Checkbox
                  checked={selectedSlot === slot_1}
                  onChange={() => handleSlotChange(slot_1)}
                />
              }
              label={formatDate(slot_1)}
            />
          )}
          {slot_2 && (
            <FormControlLabel
              control={
                <Checkbox
                  checked={selectedSlot === slot_2}
                  onChange={() => handleSlotChange(slot_2)}
                />
              }
              label={formatDate(slot_2)}
            />
          )}
          {slot_3 && (
            <FormControlLabel
              control={
                <Checkbox
                  checked={selectedSlot === slot_3}
                  onChange={() => handleSlotChange(slot_3)}
                />
              }
              label={formatDate(slot_3)}
            />
          )}
        </FormGroup>
        <Button
          variant="contained"
          color="primary"
          onClick={handleSubmit}
          style={{ marginTop: "20px" }}
        >
          Submit
        </Button>
        {selectedSlot && (
          <Typography variant="body1" style={{ marginTop: "20px" }}>
            Selected Slot: {formatDate(selectedSlot)}
          </Typography>
        )}
      </CardContent>
    </Card>
  );
};

export default ShowRequestSlot;
