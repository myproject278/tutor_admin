// RequestDetails.js
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { get_request_by_id } from "../../actions/request_action";
import {
  Typography,
  Paper,
  List,
  ListItem,
  ListItemText,
  Button,
  Card,
  CardContent,
} from "@mui/material";
import { get_image_url } from "../../helper/util";
import "./style.css";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TrackStepper from "./TrackStepper";
import ShowRequestSlot from "./ShowRequestSlot";
import { formatDate } from "../../utils/functions";

const RequestDetails = () => {
  const dispatch = useDispatch();
  const { request_id } = useParams();
  const {
    approve_time,
    email,
    goal_of_tutoring,
    id,
    request_time,
    slot_1,
    slot_2,
    slot_3,
    status,
    subject_image,
    subject_name,
    student_image,
    student_name,
    phone_no,
    total_hours,
  } = useSelector((state) => state.requestReducer.requestDetails);
  useEffect(() => {
    if (request_id) dispatch(get_request_by_id(request_id));
  }, [request_id]);

  return (
    <div className="request-details-container">
      <div elevation={3} className="request-details">
        <div
          className="top_teacher_info"
          style={{ display: "flex", justifyContent: "space-between" }}
        >
          <div className="teacher-info">
            {student_image && (
              <div className="info-image-div">
                <img src={get_image_url(student_image)} alt={subject_name} />
              </div>
            )}
            <div style={{ marginLeft: "70px" }}>
              <Typography variant="h3">{student_name}</Typography>
              <Typography variant="body1">{phone_no}</Typography>
              <Typography variant="body1">{email}</Typography>
            </div>
          </div>
          {status == 0 ? (
            <ShowRequestSlot
              slot_1={slot_1}
              slot_2={slot_2}
              slot_3={slot_3}
              total_hours={total_hours}
              request_id={id}
              student_email={email}
              subject_name={subject_name}
            />
          ) : (
            <div
              style={{
                display: "flex",
                alignItems: "flex-end",
                padding: "34px 0",
              }}
            >
              <Typography variant="h5">
                Lecture Time : &nbsp;{formatDate(approve_time)}
              </Typography>
            </div>
          )}
        </div>

        <hr />
        <Typography variant="h4" style={{ marginTop: "45px" }}>
          Request Info :{" "}
        </Typography>
        <div className="subject-info">
          {subject_image && (
            <div className="info-image-div">
              <img src={get_image_url(subject_image)} alt={subject_name} />
            </div>
          )}
          <TableContainer style={{ marginLeft: "50px" }}>
            <Table sx={{ width: 450 }} aria-label="simple table">
              <TableRow
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  Subject Name
                </TableCell>
                <TableCell align="left">:</TableCell>
                <TableCell align="left" style={{ fontSize: "18px" }}>
                  {subject_name}
                </TableCell>
              </TableRow>
              <TableRow
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  Reqeustd Time
                </TableCell>
                <TableCell align="left">:</TableCell>
                <TableCell align="left" style={{ fontSize: "18px" }}>
                  {formatDate(request_time)}
                </TableCell>
              </TableRow>
              <TableRow
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  Total Hours
                </TableCell>
                <TableCell align="left">:</TableCell>
                <TableCell align="left" style={{ fontSize: "18px" }}>
                  {total_hours} Hour
                </TableCell>
              </TableRow>
            </Table>
          </TableContainer>
        </div>

        <div className="request-info">
          <div className="bio-info">
            <Typography variant="h6">Goal Of Tutoring :</Typography>
            <Typography variant="body1">{goal_of_tutoring}</Typography>
          </div>
          <hr />
          <div
            className="sm:w-1/2 border-r"
            style={{
              alignItems: "center",
              display: "flex",
              justifyContent: "center",
              margin: "40px 0",
            }}
          >
            {" "}
            <div className="flex flex-col w-full ">
              <h3
                className="font-medium sm:text-center"
                style={{
                  marginBottom: "15px",
                }}
              >
                Request Status :
              </h3>
              <TrackStepper activeStep={status} />
            </div>{" "}
          </div>
        </div>

        <div
          style={{
            marginTop: "40px",
            padding: "30px 20px",
            display: "flex",
            justifyContent: "space-between",
            border: "2px solid #eeeeee",
          }}
        >
          <div>
            {status < 5 && (
              <Button color="error" variant="contained">
                Cancel Request
              </Button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default RequestDetails;
