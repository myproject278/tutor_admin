import { ADD_REVIEW_SUCCESS, GET_REVIEW_SUCCESS } from "../constant";


const intial_state= {
    reviews : [],
    error :null 
}

const reviewReducer = ( state= intial_state , action)=>{
    switch(action.type){
       case GET_REVIEW_SUCCESS:
        state.reviews = action.data
        break;
       case ADD_REVIEW_SUCCESS :
        state.reviews = [action.data,...state.reviews]
        break;
       default:
        return state
    }
    return {
        ...state
    }
}
export default reviewReducer