import {
  GET_NOTIFICATION_SUCESS,
  REDUCE_UNREAD_COUNT,
  SET_UNREAD_NOTIFICATION_COUNT,
} from "../constant";

const intial_state = {
  notifications: [],
  unread_count: 0,
};

const notificationReducer = (state = intial_state, action) => {
  switch (action.type) {
    case GET_NOTIFICATION_SUCESS:
      state.notifications = action.data;
      break;
    case SET_UNREAD_NOTIFICATION_COUNT:
      state.unread_count = action.count;
      break;
    case REDUCE_UNREAD_COUNT:
      state.unread_count = state.unread_count - 1;
    default:
      return state;
  }
  return {
    ...state,
  };
};
export default notificationReducer;
