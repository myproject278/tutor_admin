
import { GET_ADDRESS_FOR_USER_SUCCESS } from "../constant"

const intialState={
   address:[]
}
const addressReducer=(state=intialState,action)=>{
       switch(action.type){
           case GET_ADDRESS_FOR_USER_SUCCESS:
               state.address = action.data
               break;
       }
       return {
        ...state
       };
}

export default addressReducer