import {
  GET_REQUEST_BY_ID_SUCESS,
  GET_REQUEST_BY_USER_ID,
  GET_REQUEST_FOR_CALENDER,
} from "../constant";

const intial_state = {
  requests: [],
  requestDetails: {},
  reqeusts_for_calender: [],
  error: null,
};

const requestReducer = (state = intial_state, action) => {
  switch (action.type) {
    case GET_REQUEST_BY_USER_ID:
      state.requests = action.data;
      break;
    case GET_REQUEST_BY_ID_SUCESS:
      state.requestDetails = action.data;
      break;
    case GET_REQUEST_FOR_CALENDER:
      state.reqeusts_for_calender = action.data;
      break;
    default:
      return state;
  }
  return {
    ...state,
  };
};
export default requestReducer;
