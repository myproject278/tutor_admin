import {
  GET_SUBJECTS_FOR_TEACHER,
  GET_SUBJECT_DETAILS_FOR_TEACHER_UPDATE,
  GET_SUBJECT_TEACHER_BY_ID,
  GET_TEACHERS_BY_SUBJECT,
} from "../constant";

const intial_state = {
  teacher_list: [],
  teacher_details: {},
  teacher_subject_list: [],
  subject_details: {},
  error: null,
};

const subjectTeacherReducer = (state = intial_state, action) => {
  switch (action.type) {
    case GET_TEACHERS_BY_SUBJECT:
      state.teacher_list = action.data;
      break;
    case GET_SUBJECT_TEACHER_BY_ID:
      state.teacher_details = action.data;
      break;
    case GET_SUBJECTS_FOR_TEACHER:
      state.teacher_subject_list = action.data;
      break;
    case GET_SUBJECT_DETAILS_FOR_TEACHER_UPDATE:
      state.subject_details = action.data;
      break;

    default:
      return state;
  }
  return {
    ...state,
  };
};
export default subjectTeacherReducer;
