import { FETCH_SUBJECTS, FETCH_SUBJECTS_FOR_DROPDOWN } from "../constant";

const intial_state = {
  subjects_dropdown: [],
  error: null,
};

const subjectReducer = (state = intial_state, action) => {
  switch (action.type) {
    case FETCH_SUBJECTS_FOR_DROPDOWN:
      state.subjects_dropdown = action.data;
      break;
    default:
      return state;
  }
  return {
    ...state,
  };
};
export default subjectReducer;
