import { ADD_TRANSACTION_FROM_USER, GET_EXPENDITURE_DATE_SUCCESS, GET_OWNER_SUGGESTION,  GET_TOTAL_PAID_AMT,  GET_TOTAL_TRANSACTIONS_COUNT,  GET_TRANSACTIONS_FOR_USER, GET_USER_OWNER_DUE} from "../constant"


const intial_state= {
    transactions : [],
    owner_dues :[],
    other:{expenditure:0,paid:0},
    error :null ,
    owner_suggestions:[],
    total_records:0,
    total_paid :0
}

const transactionReducer = ( state= intial_state , action)=>{
    switch(action.type){
       case GET_TRANSACTIONS_FOR_USER:
        state.transactions = action.data
        break;
        case GET_USER_OWNER_DUE:
            state.owner_dues = action.data
            break; 
        case GET_EXPENDITURE_DATE_SUCCESS:
             state.other= {expenditure: action.data.expenditure==null?0:action.data.expenditure*-1,
            paid : action.data.paid==null?0:action.data.paid}
            break;
        case GET_OWNER_SUGGESTION:
            state.owner_suggestions=action.data
            break;
        case GET_TOTAL_TRANSACTIONS_COUNT:
            state.total_records = action.data
            break;
       case GET_TOTAL_PAID_AMT:
           state.total_paid= action.data
           break;
       default:
        return state
    }
   
    return {
        ...state
    }
}
export default transactionReducer