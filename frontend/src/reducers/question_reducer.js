import { GET_ANSWER_FOR_QUESTION, GET_QUESTIONS } from "../constant";

const intial_state = {
  questions: [],
  answers: [],
  error: null,
};

const questionReducer = (state = intial_state, action) => {
  switch (action.type) {
    case GET_QUESTIONS:
      state.questions = action.data;
      break;
    case GET_ANSWER_FOR_QUESTION:
      state.answers = action.data;
      break;

    default:
      return state;
  }
  return {
    ...state,
  };
};
export default questionReducer;
