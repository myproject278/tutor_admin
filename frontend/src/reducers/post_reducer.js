import {
  GET_POST_BY_ID,
  GET_ALL_POST_SUCCESS,
  GET_COMMNETS_SUCESS,
} from "../constant";

const intial_state = {
  posts: [],
  post_details: {},
  error: null,
  comments: [],
};

const postReducer = (state = intial_state, action) => {
  switch (action.type) {
    case GET_ALL_POST_SUCCESS:
      state.posts = action.data;
      break;
    case GET_POST_BY_ID:
      state.post_details = action.data;
      break;
    case GET_COMMNETS_SUCESS:
      state.comments = action.data;
      break;
    default:
      return state;
  }
  return {
    ...state,
  };
};
export default postReducer;
