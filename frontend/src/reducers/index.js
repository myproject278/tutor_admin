import { combineReducers } from "redux";
import subjectReducer from "./subject_reducer";
import  subjectTeacherReducer from "./subject_teacher_reducer"
import authReducer from "./auth_reducer";
import requestReducer from './request_reducer'
import wishlistReducer from "./wishlist_reducers";
import transactionReducer from './transactionReducer'
import reviewReducer from "./review_reducer";
import addressReducer from "./address_ruducer";
import teacherReducer from "./teacher_reducer";
import postReducer from "./post_reducer";
import questionReducer from "./question_reducer";
import notificationReducer from "./notification_reducer";

const reudcers = combineReducers({
  subjectReducer,
  subjectTeacherReducer,
  authReducer,
  requestReducer,
  wishlistReducer,
  transactionReducer,
  reviewReducer,
  addressReducer,
  teacherReducer,
  postReducer,
  questionReducer,
  notificationReducer,
});

export default reudcers;
