import axios from "axios";
import store from "../store/store";
import { BASE_URL } from "../constant";
const axiosAuth = () => {
  const authAxios = axios.create({
    baseURL: BASE_URL ,
    headers: {
      token: store.getState().authReducer.token,
    },
  });
  return authAxios;
};
export default axiosAuth;
